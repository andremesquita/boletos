package co.tiozao.boletos.helper;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by andre on 12/06/17.
 */

public class Utils {

    //Returns an array of Integers [0:9] for each char of the String
    public static int[] stringToIntArray(String str) {
        int[] result = new int[str.length()];

        int i = 0;
        for (char c: str.toCharArray()) {
            result[i++] = c - '0';
        }

        return result;
    }

    //Returns the sum of the values of each digit of a number, base 10
    public static int digitSum(int number){
        int sum = 0;

        while (number > 0) {
            sum += number % 10;
            number = number / 10;
        }

        return sum;
    }

    //The timestamp constant for 1997/10/07
    public static long baseTimestamp(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = null;

        try {
            date = dateFormat.parse("1997/10/07");
            long time = date.getTime();
            Timestamp timestamp = new Timestamp(time);

            return timestamp.getTime();

        } catch (ParseException e) {
            e.printStackTrace();

            return 0;
        }
    }

    public static String currencyString(long value, String format) {
       return String.format(format, value);
    }

    public static String timestampToStr(long timestamp, String format){
        Date date = new Date(timestamp);
        DateFormat dateFormat = new SimpleDateFormat(format);

        return dateFormat.format(date);
    }
}
