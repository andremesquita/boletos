package co.tiozao.boletos.model;

/**
 * Created by andre on 16/06/17.
 */

public interface LinhaDigitavel {

    void verify();

    void fetchValue();

    void fetchDeadline();

    void generateBarCode();

    boolean validadeBarCode();

    void parse();

    String getValue();

    String getDeadline(String format);

    boolean getIsValid();

    String getBarcode();
}
