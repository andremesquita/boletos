package co.tiozao.boletos.model;

/**
 * Created by andre on 12/06/17.
 */

public class Barcode {

    private String code;

    public Barcode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
