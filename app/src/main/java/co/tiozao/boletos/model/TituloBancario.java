package co.tiozao.boletos.model;

import co.tiozao.boletos.helper.Utils;

/**
 * Created by andre on 12/06/17.
 */

public class TituloBancario extends AbstractBill implements LinhaDigitavel {

    String[] fields;

    public TituloBancario(String line){
        this.line = line;

        parse();

        verify();
    }

    public void verify(){
        this.isValid = verifyField(fields[0]) && verifyField(fields[1]) && verifyField(fields[2]) && validadeBarCode();

        if (isValid) {
            fetchDeadline();
            fetchValue();
        }
    }

    public void fetchValue(){
        long rawValue = Long.parseLong(fields[4].substring(4));

        this.value = rawValue;
    }

    public void fetchDeadline() {
        long days = Integer.parseInt(fields[4].substring(0, 4));

        this.date = Utils.baseTimestamp() + (days * 24 * 60 * 60 * 1000);
    }

    public void generateBarCode(){
        String barcode = fields[0].substring(0, 4) + fields[3] + fields[4] + fields[0].substring(4, 9) + fields[1].substring(0, 10) + fields[2].substring(0, 10);

        this.barcode = new Barcode(barcode);
    }

    public boolean validadeBarCode() {
        if(this.barcode == null) {
            return false;
        }

        return base11Validation(4);

    }

    private boolean verifyField(String field) {

        int multiplierShift = (field.length()+1) % 2;

        int[] algarisms = Utils.stringToIntArray(field);

        int sum = 0;

        for(int i = 0; i < algarisms.length - 1; i++) {
            int multiplier = (1 + (multiplierShift++) % 2);
            sum += Utils.digitSum(multiplier *  algarisms[i]);
        }

        int verificationDigit = 10 - (sum % 10);

        return verificationDigit == algarisms[algarisms.length - 1];
    }

    public void parse(){
        this.fields = new String[5];
        fields[0] = line.substring(0, 10);
        fields[1] = line.substring(10, 21);
        fields[2] = line.substring(21, 32);
        fields[3] = line.substring(32, 33);
        fields[4] = line.substring(33, 47);

        generateBarCode();
    }

    protected boolean base11Validation(int verifyDigitIndex){
        int[] digits = Utils.stringToIntArray( this.barcode.getCode().substring(0, verifyDigitIndex) + this.barcode.getCode().substring(verifyDigitIndex + 1) );

        int sum = 0;
        int weight = 0;

        for (int i = digits.length - 1; i >= 0; i--) {
            sum += digits[i] * (2 + (weight++ % 8));
        }

        int verifyDigit = 11 - (sum % 11);

        if(verifyDigit == 0 || verifyDigit == 10 || verifyDigit == 11 ) {
            verifyDigit = 1;
        }

        return verifyDigit == Integer.parseInt(this.barcode.getCode().substring(verifyDigitIndex, verifyDigitIndex+1));
    }
}
