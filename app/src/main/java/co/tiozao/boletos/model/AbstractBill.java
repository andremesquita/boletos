package co.tiozao.boletos.model;

import java.text.NumberFormat;
import java.util.Locale;

import co.tiozao.boletos.helper.Utils;

/**
 * Created by andre on 16/06/17.
 */

public abstract class AbstractBill {

    protected String line;
    protected boolean isValid;
    protected long value;
    protected long date;
    protected Barcode barcode;

    public String getValue(){
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.getDefault());
        return n.format(value / 100.0);
    }

    public String getDeadline(String format){
        return Utils.timestampToStr(date, format);
    }

    public boolean getIsValid(){
        return isValid;
    }

    public String getBarcode(){
        if(barcode != null) {
            return barcode.getCode();
        }

        return null;
    }

}