package co.tiozao.boletos.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import co.tiozao.boletos.helper.Utils;

/**
 * Created by andre on 16/06/17.
 */

public class PagamentoConcessionaria extends AbstractBill implements LinhaDigitavel {

    String[] fields;

    public PagamentoConcessionaria(String line){
        this.line = line;

        parse();

        verify();
    }

    public void verify(){
        this.isValid = verifyField(fields[0]) && verifyField(fields[1]) && verifyField(fields[2]) && verifyField(fields[3]) && validadeBarCode();


        if (isValid) {
            fetchDeadline();
            fetchValue();
        }
    }

    public void fetchValue(){
        long rawValue = Long.parseLong(line.substring(5, 15));

        this.value = rawValue;
    }

    public void fetchDeadline() {
        String dateString = barcode.getCode().substring(20, 28);

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = null;

        try {
            date = dateFormat.parse(dateString);
            long time = date.getTime();
            Timestamp timestamp = new Timestamp(time);

            this.date = timestamp.getTime();

        } catch (ParseException e) {
            e.printStackTrace();

            this.date = 0;
        }
    }

    public void generateBarCode(){
        String barcode = fields[0].substring(0, 11) + fields[1].substring(0, 11) + fields[2].substring(0, 11) + fields[3].substring(0, 11);

        this.barcode = new Barcode(barcode);
    }

    public boolean validadeBarCode() {
        if(this.barcode == null) {
            return false;
        }

        return base10Validation(3);

    }

    private boolean verifyField(String field) {

        int multiplierShift = (field.length()+1) % 2;

        int[] algarisms = Utils.stringToIntArray(field);

        int sum = 0;

        for(int i = 0; i < algarisms.length - 1; i++) {
            int multiplier = (1 + (multiplierShift++) % 2);
            sum += Utils.digitSum(multiplier *  algarisms[i]);
        }

        int verificationDigit = 10 - (sum % 10);

        if(verificationDigit == 10) {
            verificationDigit = 0;
        }

        return verificationDigit == algarisms[algarisms.length - 1];
    }

    public void parse(){
        this.fields = new String[4];
        fields[0] = line.substring(0, 12);
        fields[1] = line.substring(12, 24);
        fields[2] = line.substring(24, 36);
        fields[3] = line.substring(36, 48);

        generateBarCode();
    }

    protected boolean base10Validation(int verifyDigitIndex){
        int[] digits = Utils.stringToIntArray( this.barcode.getCode().substring(0, verifyDigitIndex) + this.barcode.getCode().substring(verifyDigitIndex + 1) );

        int multiplierShift = digits.length % 2;

        int sum = 0;

        for(int i = 0; i < digits.length - 1; i++) {
            int multiplier = (1 + (multiplierShift++) % 2);
            sum += Utils.digitSum(multiplier *  digits[i]);
        }

        int verificationDigit = 10 - sum % 10;

        if (verificationDigit == 10) {
            verificationDigit = 0;
        }

        return verificationDigit == Integer.parseInt(this.barcode.getCode().substring(verifyDigitIndex, verifyDigitIndex + 1));
    }
}
