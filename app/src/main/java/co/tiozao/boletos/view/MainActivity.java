package co.tiozao.boletos.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import co.tiozao.boletos.R;
import co.tiozao.boletos.model.LinhaDigitavel;
import co.tiozao.boletos.model.PagamentoConcessionaria;
import co.tiozao.boletos.model.TituloBancario;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.evaluate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verify(((EditText)findViewById(R.id.input)).getText().toString());
            }
        });
    }

    void verify(String codeStr) {
        String digits = codeStr.replaceAll("[^0-9]", "");

        ((EditText)findViewById(R.id.input)).setText(digits);

        if(digits.length() == 47) {
            TituloBancario tituloBancario = new TituloBancario(digits);
            fillData(tituloBancario);
        } else if (digits.length() == 48) {
            PagamentoConcessionaria pagamentoConcessionaria = new PagamentoConcessionaria(digits);
            fillData(pagamentoConcessionaria);
        } else {
            Toast.makeText(this, "Entrada não reconhecida",
                    Toast.LENGTH_LONG).show();
        }
    }

    void fillData(LinhaDigitavel linhaDigitavel) {
        ((TextView)findViewById(R.id.status)).setText(String.format(getResources().getString(R.string.status_label), linhaDigitavel.getIsValid()));

        if(linhaDigitavel.getIsValid()) {

            ((TextView)findViewById(R.id.value)).setText(linhaDigitavel.getValue());

            if(linhaDigitavel instanceof TituloBancario) {
                ((TextView) findViewById(R.id.deadline)).setText(linhaDigitavel.getDeadline(getResources().getString(R.string.date_format)));
            } else {
                ((TextView)findViewById(R.id.deadline)).setText("");
            }
            ((TextView)findViewById(R.id.barcode)).setText(linhaDigitavel.getBarcode());
        } else {
            ((TextView)findViewById(R.id.value)).setText("");
            ((TextView)findViewById(R.id.deadline)).setText("");
            ((TextView)findViewById(R.id.barcode)).setText("");
        }


    }
}
